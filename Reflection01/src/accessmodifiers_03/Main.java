package accessmodifiers_03;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        Class<Car> car = Car.class;

        System.out.println(car.getSimpleName());

        System.out.println("Fields");
        Field[] fields = car.getFields();
        Arrays.stream(fields).forEach(System.out::println);

        System.out.println("Declared Fields");
        Field[] declaredFields = car.getDeclaredFields();
        Arrays.stream(declaredFields).forEach(System.out::println);

        System.out.println("Methods");
        Method[] methods = car.getMethods();
        Arrays.stream(methods).forEach(System.out::println);

        System.out.println("Declared Methods");
        Method[] declaredMethods = car.getDeclaredMethods();
        Arrays.stream(declaredMethods).forEach(System.out::println);

        System.out.println("Constructors");
        Constructor<?>[] constructors = car.getConstructors();
        Arrays.stream(constructors).forEach(System.out::println);

        System.out.println("Declared Constructors");
        Constructor<?>[] declaredConstructors = car.getDeclaredConstructors();
        Arrays.stream(declaredConstructors).forEach(System.out::println);


    }

}
