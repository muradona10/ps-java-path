package accessmodifiers_03;

public class Car {

    public String type;

    private String brand;

    private String model;

    protected int speed;

    public void speedUp(int rate){
        speed += rate;
    }

    private void stop(){
        speed = 0;
    }

    public Car(){}

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
