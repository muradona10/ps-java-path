package callmethod_04;

public class BankAccount {

    private int unit;
    private int account;
    private double balance;

    public BankAccount(){}

    public BankAccount(int unit, int account, double balance){
        this.unit = unit;
        this.account = account;
        this.balance = balance;
    }

    public void deposit(double money){
        balance += money;
    }

    public void withdraw(double money){
        if(money > balance)
            return;

        balance -= money;
    }

    private void unitAccountTransfer(int newUnit, int newAccount){
        this.unit = newUnit;
        this.account = newAccount;
    }

    public int getUnit() {
        return unit;
    }

    public void setUnit(int unit) {
        this.unit = unit;
    }

    public int getAccount() {
        return account;
    }

    public void setAccount(int account) {
        this.account = account;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
}
