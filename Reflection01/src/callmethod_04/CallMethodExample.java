package callmethod_04;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

public class CallMethodExample {

    public static void main(String[] args) {

        BankAccount bankAccount = new BankAccount(295, 6755499, 1200.50);

        CallMethodExample callMethodExample = new CallMethodExample();
        callMethodExample.callSimpleGetter(bankAccount);

        System.out.println();

        callMethodExample.callDeposit(bankAccount, 1200.50);
        callMethodExample.callSimpleGetter(bankAccount);

        System.out.println();

        callMethodExample.transferAccount(bankAccount, 666, 6060999);
        System.out.println(bankAccount.getUnit());
        System.out.println(bankAccount.getAccount());

        System.out.println();

        /**
         * Create instance via reflection ana call method to modify it
         */
        try {
            Class<?> clazz = Class.forName("callmethod_04.BankAccount");
            Constructor<?> constructor = clazz.getConstructor(int.class, int.class, double.class);
            Object obj = constructor.newInstance(295, 6578123, 50000.00);
            Method method = clazz.getMethod("deposit", double.class);
            method.invoke(obj, 50000.00);
            BankAccount ba = (BankAccount) obj;
            System.out.println(ba.getBalance());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Call a simple public method by reflection
     * Just use methodName
     * @param obj
     */
    public void callSimpleGetter(Object obj) {

        try {
            Class<?> clazz = obj.getClass();
            Method method = clazz.getMethod("getBalance");
            Object invoke = method.invoke(obj);
            System.out.println(invoke);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Call a public method that takes one parameter via reflection
     * Use method name and parameter class
     * @param obj
     * @param money
     */
    public void callDeposit(Object obj, double money){

        try {
            Class<?> clazz = obj.getClass();
            Method method = clazz.getMethod("deposit", double.class);
            method.invoke(obj, money);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Call a PRIVATE method via reflection
     * Attention..! we take method and set accessible this method before we invoke it
     * @param obj
     * @param newUnit
     * @param newAccount
     */
    public void transferAccount(Object obj, int newUnit, int newAccount){

        try {
            Class<?> clazz = obj.getClass();
            Method method = clazz.getDeclaredMethod("unitAccountTransfer", int.class, int.class);
            method.setAccessible(true);
            method.invoke(obj, newUnit, newAccount);
            method.setAccessible(false);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
