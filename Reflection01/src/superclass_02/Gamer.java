package superclass_02;

import typeclass_01.Player;

public class Gamer extends Player implements Playable {

    @Override
    public void play() {
        System.out.println("Let's play the game...");
    }

}
