package superclass_02;

public class SuperClassAndInterfaces {

    public static void main(String[] args) {

        Class<?> gamer = Gamer.class;
        System.out.println(gamer.getSimpleName());

        Class<?> player = gamer.getSuperclass();
        System.out.println(player.getSimpleName());

        Class<?>[] interfaces = gamer.getInterfaces();
        for (Class<?> in : interfaces){
            System.out.println(in.getSimpleName());
        }


        int modifiers = gamer.getModifiers();
        System.out.println(modifiers);

    }

}
