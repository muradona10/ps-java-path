package typeclass_01;

public class TypeClass {

    public static void main(String[] args) throws ClassNotFoundException {

        Class<?> c1 = Class.forName("typeclass_01.Player");
        System.out.println(c1.getSimpleName());

        Class<?> c2 = Player.class;
        System.out.println(c2.getSimpleName());

        Class<Player> c3 = Player.class;
        System.out.println(c3.getSimpleName());

    }

}
