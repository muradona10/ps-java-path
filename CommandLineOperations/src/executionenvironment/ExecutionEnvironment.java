package executionenvironment;


public class ExecutionEnvironment {

    public static void main(String[] args) {

        String userName = System.getProperty("user.name");
        String userHome = System.getProperty("user.home");
        String osArch = System.getProperty("os.arch");
        String javaVendor = System.getProperty("java.vendor");

        System.out.println(userName);
        System.out.println(userHome);
        System.out.println(osArch);
        System.out.println(javaVendor);

    }

}
