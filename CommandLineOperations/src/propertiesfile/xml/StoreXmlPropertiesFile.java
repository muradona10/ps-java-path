package propertiesfile.xml;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

public class StoreXmlPropertiesFile {

    public static void main(String[] args) {

        Properties prop = new Properties();
        Path path = Paths.get("xml_prop.xml");

        prop.setProperty("ValentinoRossi","Yamaha");
        prop.setProperty("MarkMarquez","Honda");
        prop.setProperty("JackMiller","Ducati");
        prop.setProperty("AlexRins","Suziki");

        writeToXmlProperties(prop, path);

    }

    private static void writeToXmlProperties(Properties prop, Path path) {

        try (OutputStream os = Files.newOutputStream(path)) {
            prop.storeToXML(os, "xmlProperties");
        } catch (IOException e){
            System.out.println(e.getMessage());
        }

    }

}
