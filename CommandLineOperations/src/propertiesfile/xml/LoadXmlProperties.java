package propertiesfile.xml;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

public class LoadXmlProperties {

    public static void main(String[] args) {

        Properties prop = new Properties();
        Path path = Paths.get("load_xml.xml");

        loadFromXmlProperties(path, prop);

    }

    private static void loadFromXmlProperties(Path path, Properties prop) {

        try (InputStream in = Files.newInputStream(path)) {
            prop.loadFromXML(in);
        } catch (IOException e){
            System.out.println(e.getMessage());
        }

        prop.entrySet().stream().forEach(entry -> {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        });

    }

}
