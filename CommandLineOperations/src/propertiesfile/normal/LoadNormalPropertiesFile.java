package propertiesfile.normal;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

public class LoadNormalPropertiesFile {

    public static void main(String[] args) {

        Properties prop = new Properties();
        Path path = Paths.get("load_default.properties");

        loadFromProperties(path, prop);

    }

    private static void loadFromProperties(Path path, Properties prop) {

        try (Reader r = Files.newBufferedReader(path)) {
            prop.load(r);
        } catch (IOException e){
            System.out.println(e.getMessage());
        }

        for (String s :prop.stringPropertyNames()){
            System.out.println(s + ": " + prop.getProperty(s));
        }

    }

}
