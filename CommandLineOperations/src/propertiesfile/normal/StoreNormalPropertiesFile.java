package propertiesfile.normal;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

public class StoreNormalPropertiesFile {

    public static void main(String[] args) {

        Properties prop = new Properties();
        Path path = Paths.get("default.properties");

        prop.setProperty("ValentinoRossi","Yamaha");
        prop.setProperty("MarkMarquez","Honda");
        prop.setProperty("JackMiller","Ducati");
        prop.setProperty("AlexRins","Suziki");

        writeToProperties(prop,path);

    }

    private static void writeToProperties(Properties prop, Path path) {

        try (Writer w = Files.newBufferedWriter(path)) {
            prop.store(w,"deault properties");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }
}
