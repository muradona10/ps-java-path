package cmd;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CommandLine {

    public static void main(String[] args) {

        if(args.length == 0){
            return;
        }

        String fileName = args[0];
        Path filePath = Paths.get(fileName);
        if(!Files.exists(filePath)){
            System.out.println("There is no file specified name..!: " + fileName);
        } else {

            try {
                Files.lines(filePath, StandardCharsets.UTF_8).forEach(System.out::println);
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }

        }

    }
}
