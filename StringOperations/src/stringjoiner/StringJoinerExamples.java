package stringjoiner;

import java.util.StringJoiner;

public class StringJoinerExamples {

    public static void lineSeprerator(){
        System.out.println("___________________________________________________");
    }

    public static void main(String[] args) {

        // Simple String Joiner
        StringJoiner sj1 = new StringJoiner(", ");
        sj1.add("A");
        sj1.add("B");
        sj1.add("C");
        sj1.add("D");
        System.out.println(sj1.toString()); // A, B, C, D

        lineSeprerator();

        // String Joiner with chain
        StringJoiner sj2 = new StringJoiner(" | ");
        sj2.add("A").add("B").add("C").add("D");
        System.out.println(sj2.toString()); // A | B | C | D

        lineSeprerator();

        // String Joiner with prefix and suffix
        StringJoiner sj3 = new StringJoiner("], [","[","]");
        sj3.add("A").add("B").add("C").add("D");
        System.out.println(sj3.toString()); // [A],[B],[C],[D]

        lineSeprerator();

        // EDGE CASES
        StringJoiner sj4 = new StringJoiner(",");
        sj4.add("Murat");
        System.out.println(sj4.toString()); // Murat
        StringJoiner sj5 = new StringJoiner(",","{","}");
        sj5.add("Murat");
        System.out.println(sj5.toString()); // {Murat}

        lineSeprerator();

        // EMPTY
        StringJoiner sj6 = new StringJoiner(",");
        System.out.println(sj6.toString()); // nothing
        StringJoiner sj7 = new StringJoiner(",","{","}");
        System.out.println(sj7.toString()); // {}

        lineSeprerator();

        // SET EMPTY
        StringJoiner sj8 = new StringJoiner(",");
        sj8.setEmptyValue("EMPTY");
        System.out.println(sj8.toString()); // EMPTY
        StringJoiner sj9 = new StringJoiner(",","{","}");
        sj9.setEmptyValue("EMPTY");
        System.out.println(sj9.toString()); // EMPTY

        lineSeprerator();

        // "" empty string
        StringJoiner sj10 = new StringJoiner(",");
        sj10.add("");
        sj10.setEmptyValue("EMPTY");
        System.out.println(sj10.toString()); // nothing
        StringJoiner sj11 = new StringJoiner(",","{","}");
        sj11.add("");
        sj11.setEmptyValue("EMPTY");
        System.out.println(sj11.toString()); // {}



    }

}
