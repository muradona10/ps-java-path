package read;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class FilesReadAllLines_Example {

    public void readFile(Path path){

        try  {
            List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);
            lines.forEach(System.out::println);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }

    public static void main(String[] args) {
        FilesReadAllLines_Example fReadAll = new FilesReadAllLines_Example();
        Path path = Paths.get("stars.txt");
        fReadAll.readFile(path);
    }
}
