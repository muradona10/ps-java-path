package read;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class FilesLines_Example {

    public void readFile(Path path){

        try {
            Stream<String> lines = Files.lines(path, StandardCharsets.UTF_8);
            lines.map(String::toUpperCase).forEach(System.out::println);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void main(String[] args) {
        FilesLines_Example fLines = new FilesLines_Example();
        Path path = Paths.get("stars.txt");
        fLines.readFile(path);
    }

}
