package read;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class BufferedInputStreamReader_Example {

    public void readFile(Path source, Path target){

        try (InputStream inputStreamReader = Files.newInputStream(source);
             BufferedInputStream bis = new BufferedInputStream(inputStreamReader);

             OutputStream outputStream = Files.newOutputStream(target);
             BufferedOutputStream bos = new BufferedOutputStream(outputStream);
             ) {

            byte[] buffer = new byte[4096];
            int numberOfBytes;
            while ((numberOfBytes = bis.read(buffer)) != -1){
                bos.write(buffer, 0, numberOfBytes);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        Path source = Paths.get("team.png");
        Path target = Paths.get("copy_of_team.png");

        BufferedInputStreamReader_Example bOut = new BufferedInputStreamReader_Example();
        bOut.readFile(source,target);
    }

}
