package read;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class BufferedReader_Example {

    public void readFile(Path path){

        try (BufferedReader br = Files.newBufferedReader(path, StandardCharsets.UTF_8)){
            String line;
            while ((line=br.readLine())!=null){
                System.out.println(line);
            }
        } catch (IOException ex){
            System.out.println(ex.getMessage());
        }

    }

    public static void main(String[] args) {
        BufferedReader_Example brExample = new BufferedReader_Example();

        Path path = Paths.get("stars.txt");
        brExample.readFile(path);
    }

}
