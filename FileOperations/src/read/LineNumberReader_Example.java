package read;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class LineNumberReader_Example {

    public void readFile(Path path){

        try ( BufferedReader bufferedReader = Files.newBufferedReader(path,StandardCharsets.UTF_8))
        {
            LineNumberReader lineNumberReader = new LineNumberReader(bufferedReader);
            String line;
            while ((line = lineNumberReader.readLine()) != null) {
                System.out.format("Line %2d: %40s%n", lineNumberReader.getLineNumber(), line);
            }
        } catch (IOException e){
            System.out.println(e.getMessage());
        }
    }

    public static void main(String[] args) {

        LineNumberReader_Example lnrExample = new LineNumberReader_Example();
        Path path = Paths.get("stars.txt");
        lnrExample.readFile(path);

    }
}
