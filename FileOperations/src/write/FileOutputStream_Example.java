package write;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class FileOutputStream_Example {

    public void writeToFile(List<String> lines, Path path){

        try (FileOutputStream fos = new FileOutputStream(path.toString())) {
            for(String line : lines) {
                fos.write((line + "\n").getBytes());
            }
            fos.flush();
        } catch (IOException ex){
            System.out.println(ex.getMessage());
        }

    }

    public static void main(String[] args) {
        Path path = Paths.get("05_FileOutputStream.txt");
        List<String> lines = Arrays.asList("Fenerbahçe","Galatasaray","Beşiktaş","Trabzonspor");
        FileOutputStream_Example fos = new FileOutputStream_Example();
        fos.writeToFile(lines, path);
    }

}
