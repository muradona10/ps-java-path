package write;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class FileWriter_Example {

    public void writeToFile(List<String> lines, Path path){

        try (FileWriter fw = new FileWriter(path.toString())) {
            for (String line : lines){
                fw.write(line + "\n");
            }
        } catch (IOException ex){
            System.out.println(ex.getMessage());
        }

    }

    public static void main(String[] args) {
        Path path = Paths.get("03_FileWriter.txt");
        List<String> lines = Arrays.asList("Fenerbahçe","Galatasaray","Beşiktaş","Trabzonspor");
        FileWriter_Example fw = new FileWriter_Example();
        fw.writeToFile(lines, path);
    }

}
