package write;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Nio_FilesWrite_Example {

    public void writeToFile(List<String> lines, Path path) throws IOException {

        String data = lines.stream().collect(Collectors.joining("\n"));
        Files.write(path, data.getBytes());

    }

    public static void main(String[] args) throws IOException {
        Path path = Paths.get("06_FilesWrite.txt");
        List<String> lines = Arrays.asList("Fenerbahçe","Galatasaray","Beşiktaş","Trabzonspor");
        Nio_FilesWrite_Example nio = new Nio_FilesWrite_Example();
        nio.writeToFile(lines, path);
    }

}
