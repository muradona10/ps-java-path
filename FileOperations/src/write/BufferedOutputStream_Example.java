package write;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class BufferedOutputStream_Example {

    public void writeToFile(List<String> lines, Path path){

        try (OutputStream os = Files.newOutputStream(path)) {
            BufferedOutputStream bos = new BufferedOutputStream(os);
            for (String line : lines){
                bos.write((line + "\n").getBytes());
            }
            bos.flush();
        } catch (IOException ex){
            System.out.println(ex.getMessage());
        }

    }

    public static void main(String[] args) {
        Path path = Paths.get("04_BufferedOutputStream.txt");
        List<String> lines = Arrays.asList("Fenerbahçe","Galatasaray","Beşiktaş","Trabzonspor");
        BufferedOutputStream_Example bos = new BufferedOutputStream_Example();
        bos.writeToFile(lines, path);
    }

}
