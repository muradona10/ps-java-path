package write;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class PrintWriter_Example {

    public void writeToFile(List<String> lines, Path path){

        try (BufferedWriter bw = Files.newBufferedWriter(path, StandardCharsets.UTF_8)) {
            PrintWriter pw = new PrintWriter(bw);
            for(String line : lines){
                pw.printf("%40s", line);
                pw.println();
            }
        } catch (IOException ex){
            System.out.println(ex.getMessage());
        }

    }

    public static void main(String[] args) {
        Path path = Paths.get("02_PrintWriter.txt");
        List<String> lines = Arrays.asList("Fenerbahçe","Galatasaray","Beşiktaş","Trabzonspor");
        PrintWriter_Example pw = new PrintWriter_Example();
        pw.writeToFile(lines, path);
    }

}
