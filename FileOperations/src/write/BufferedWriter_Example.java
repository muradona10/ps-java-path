package write;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class BufferedWriter_Example {

    public void writeToFile(List<String> lines, Path path) {

        try (BufferedWriter bw = Files.newBufferedWriter(path, StandardCharsets.UTF_8)) {
            for (String item : lines) {
                bw.write(item);
                bw.newLine();
            }
        } catch (IOException ex){
            System.out.println(ex.getMessage());
        }

    }

    public static void main(String[] args) {
        Path path = Paths.get("01_BufferedWriter.txt");
        List<String> lines = Arrays.asList("Fenerbahçe","Galatasaray","Beşiktaş","Trabzonspor");
        BufferedWriter_Example bwExample = new BufferedWriter_Example();
        bwExample.writeToFile(lines,path);
    }

}
