package ThreadExample_03_Future;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Callable;

public class Adder2 implements Callable {

    private String inFile;

    public Adder2(String inFile){
        this.inFile = inFile;
    }

    public int doAdd(){

        int total = 0;

        try (BufferedReader br = Files.newBufferedReader(Paths.get(inFile))) {
            String line = null;
            while ((line = br.readLine()) != null){
                total += Integer.parseInt(line);
            }
        } catch (IOException e){
            System.out.println(e.getMessage());
        }

        return total;
    }

    @Override
    public Object call() throws Exception {
        return doAdd();
    }
}
