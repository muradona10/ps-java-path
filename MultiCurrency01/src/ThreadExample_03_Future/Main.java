package ThreadExample_03_Future;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Main {

    public static void main(String[] args) {

        String[] inFiles = {"file1.txt", "file2.txt", "file3.txt", "file4.txt", "file5.txt", "file6.txt"};

        ExecutorService es = Executors.newFixedThreadPool(3);
        Future<Integer>[] futures = new Future[inFiles.length];

        for (int i = 0; i < inFiles.length; i++) {
            Adder2 adder2 = new Adder2(inFiles[i]);
            futures[i] = es.submit(adder2);
        }

        for (Future future : futures){
            try {
                int result = (int) future.get();
                System.out.println(result);
            } catch (InterruptedException | ExecutionException e) {
                System.out.println(e.getMessage());
            }
        }

    }

}
