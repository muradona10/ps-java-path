package ThreadExample_02_ThreadPool;

import ThreadExample_01.Adder;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) {

        String[] inFiles = {"file1.txt","file2.txt","file3.txt","file4.txt","file5.txt","file6.txt"};
        String[] outFiles = {"out1.txt","out2.txt","out3.txt","out4.txt","out5.txt","out6.txt"};

        ExecutorService es = Executors.newFixedThreadPool(3);

        for (int i = 0 ; i < inFiles.length ; i++ ){
            Adder adder = new Adder(inFiles[i], outFiles[i]);
            es.submit(adder);
        }

        try {
            es.shutdown();
            es.awaitTermination(60, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }

    }

}
