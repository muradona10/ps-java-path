package ThreadExample_01;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Adder implements Runnable {

    private String inFile;
    private String outFile;

    public Adder(String inFile, String outFile){
        this.inFile = inFile;
        this.outFile = outFile;
    }

    public void doAdd(){

        int total = 0;

        try (BufferedReader br = Files.newBufferedReader(Paths.get(inFile));
             BufferedWriter bw = Files.newBufferedWriter(Paths.get(outFile))
        ) {

            String line = null;
            while ( (line = br.readLine()) != null ){
                total += Integer.parseInt(line);
                bw.write(String.valueOf(total));
                bw.newLine();
            }

        } catch (IOException e){
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void run() {
        doAdd();
    }
}
