package ThreadExample_01;

public class Main {

    public static void main(String[] args) {

        String[] inFiles = {"file1.txt","file2.txt","file3.txt","file4.txt","file5.txt","file6.txt"};
        String[] outFiles = {"out1.txt","out2.txt","out3.txt","out4.txt","out5.txt","out6.txt"};

        Thread[] threads = new Thread[inFiles.length];

        for (int i = 0 ; i < inFiles.length ; i++){
            Adder adder = new Adder(inFiles[i], outFiles[i]);
            threads[i] = new Thread(adder);
            threads[i].start();
        }

        // for block all thread completion
        for (Thread thread : threads){
            try {
                thread.join();
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }

    }

}
