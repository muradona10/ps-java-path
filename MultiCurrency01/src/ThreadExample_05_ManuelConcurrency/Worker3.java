package ThreadExample_05_ManuelConcurrency;

import ThreadExample_04_Concurrency.BankAccount;

public class Worker3 implements Runnable {

    private BankAccount bankAccount;

    public Worker3(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    @Override
    public void run() {

        for (int i = 0; i < 10; i++) {
            Long startBalance = bankAccount.getBalance();

            synchronized (bankAccount){
                bankAccount.deposit(Long.valueOf(10));
            }

            Long endBalance = bankAccount.getBalance();
            System.out.println("Start balance:" + startBalance + " End balance:" + endBalance);
        }

    }
}
