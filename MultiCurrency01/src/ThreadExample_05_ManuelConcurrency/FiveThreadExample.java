package ThreadExample_05_ManuelConcurrency;

import ThreadExample_04_Concurrency.BankAccount;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FiveThreadExample {

    public static void main(String[] args) {

        BankAccount bankAccount = new BankAccount(Long.valueOf(100));
        ExecutorService es = Executors.newFixedThreadPool(5);

        for (int i = 0; i < 5; i++) {
            Worker3 worker = new Worker3(bankAccount);
            es.submit(worker);
        }

    }

}
