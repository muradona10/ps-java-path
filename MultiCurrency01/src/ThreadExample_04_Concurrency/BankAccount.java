package ThreadExample_04_Concurrency;

public class BankAccount {

    private Long balance;

    public BankAccount(Long balance){
        this.balance = balance;
    }

    public Long getBalance(){
        return balance;
    }

    public void deposit(Long money){
        balance+=money;
    }

}
