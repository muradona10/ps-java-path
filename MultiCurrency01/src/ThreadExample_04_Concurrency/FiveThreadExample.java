package ThreadExample_04_Concurrency;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FiveThreadExample {

    public static void main(String[] args) {

        BankAccount2 bankAccount = new BankAccount2(Long.valueOf(100));

        ExecutorService es = Executors.newFixedThreadPool(5);

        for (int i = 0; i < 5; i++) {
            Worker2 worker = new Worker2(bankAccount);
            es.submit(worker);
        }

    }

}
