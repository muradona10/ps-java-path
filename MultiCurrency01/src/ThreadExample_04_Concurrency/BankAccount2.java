package ThreadExample_04_Concurrency;

public class BankAccount2 {

    private Long balance;

    public BankAccount2(Long balance){
        this.balance = balance;
    }

    public synchronized Long getBalance(){
        return balance;
    }

    public synchronized void deposit(Long money){
        balance+=money;
    }
}
