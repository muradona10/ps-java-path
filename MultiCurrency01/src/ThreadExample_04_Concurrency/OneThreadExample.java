package ThreadExample_04_Concurrency;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class OneThreadExample {

    public static void main(String[] args) {

        BankAccount bankAccount = new BankAccount(Long.valueOf(100));

        ExecutorService es = Executors.newFixedThreadPool(5);
        Worker worker = new Worker(bankAccount);

        es.submit(worker);

    }

}
