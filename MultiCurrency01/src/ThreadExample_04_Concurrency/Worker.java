package ThreadExample_04_Concurrency;

public class Worker implements Runnable {

    private BankAccount bankAccount;

    public Worker(BankAccount account) {
        this.bankAccount = account;
    }

    @Override
    public void run() {

        for (int i = 0; i < 10; i++) {
            Long startBalance = bankAccount.getBalance();
            bankAccount.deposit(Long.valueOf(10));
            Long endBalance = bankAccount.getBalance();
            System.out.println("Start: " + startBalance.toString()
                    + " End: " + endBalance.toString());
        }
    }
}
